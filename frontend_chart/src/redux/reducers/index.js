import themeReducer from './themeReducer';
import sidebarReducer from './sidebarReducer';
import LoginReducer from './loginReducer';
export {
  themeReducer,
  sidebarReducer,
  LoginReducer
};
